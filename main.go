package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"

	conf "gitlab.com/inquizarus/whassistant/configuration"
)

// Response ...
type Response struct {
	Message string `json:"message"`
}

var configFlag = flag.String("config", ".whassistant.conf.json", "Determines where to load whassistant config from")

func main() {
	flag.Parse()

	f, err := os.Open(*configFlag)
	check(err)
	defer f.Close()

	reader := bufio.NewReader(f)

	jsonConfig, err := conf.LoadJSONConfiguration(reader)
	check(err)

	config := conf.BuildStandardConfigurationFromJSONConfiguration(jsonConfig)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		handler(w, r, config)
	})

	printBootMessage(config)

	log.Fatal(http.ListenAndServe(fmt.Sprintf("%s:%s", "", config.GetPort()), nil))
}

func handler(w http.ResponseWriter, r *http.Request, c conf.StandardConfiguration) {
	// Just push out a standard response since webhooks usually expects it
	w.Header().Set("Content-Type", "application/json")
	bodyEncoder := json.NewEncoder(w)
	bodyEncoder.Encode(makeStandardResponse)
}

func printBootMessage(c conf.StandardConfiguration) {
	fmt.Printf(
		"booted up whassistant at port %s with main token %s\n",
		c.GetPort(),
		c.GetToken(),
	)
}

func makeStandardResponse() Response {
	return Response{Message: "ok"}
}

func check(err error) {
	if nil != err {
		panic(err)
	}
}
