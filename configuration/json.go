package configuration

import (
	"encoding/json"
	"io"
	"io/ioutil"
)

// JSONProject ...
type JSONProject struct {
	Token string `json:"token"`
	Path  string `json:"path"`
	Name  string `json:"name"`
}

// JSONConfiguration enables loading config from json file
type JSONConfiguration struct {
	Token    string        `json:"token"`
	Port     string        `json:"port"`
	Projects []JSONProject `json:"projects"`
}

// LoadJSONConfiguration ...
func LoadJSONConfiguration(r io.Reader) (JSONConfiguration, error) {
	var config JSONConfiguration
	data, err := ioutil.ReadAll(r)
	err = json.Unmarshal(data, &config)
	return config, err
}

// BuildStandardConfigurationFromJSONConfiguration ...
func BuildStandardConfigurationFromJSONConfiguration(c JSONConfiguration) StandardConfiguration {
	projects := []StandardProject{}
	for _, jsonp := range c.Projects {
		projects = append(projects, StandardProject{
			name:  jsonp.Name,
			token: jsonp.Token,
			path:  jsonp.Path,
		})
	}
	return StandardConfiguration{
		token:    c.Token,
		port:     c.Port,
		projects: projects,
	}
}
