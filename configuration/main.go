package configuration

import (
	"io"
)

// Project represents a project definition in the configuration
type Project interface {
	GetToken() string
	GetPath() string
	GetName() string
}

// StandardProject ...
type StandardProject struct {
	token string
	path  string
	name  string
}

// GetToken implementation for JSONProject
func (p StandardProject) GetToken() string {
	return p.token
}

// GetPath implementation for StandardProject
func (p StandardProject) GetPath() string {
	return p.path
}

// GetName implementation for StandardProject
func (p StandardProject) GetName() string {
	return p.name
}

// Configuration is the interface for all config alternatives
type Configuration interface {
	Load(r io.Reader) error
	GetToken() string
	GetPort() string
	GetProjects() []StandardProject
}

// StandardConfiguration ...
type StandardConfiguration struct {
	token    string
	port     string
	projects []StandardProject
}

// Load implementation for JSONConfiguration
func (c StandardConfiguration) Load(r io.Reader) error {
	return nil
}

// GetToken implementation for JSONConfiguration
func (c StandardConfiguration) GetToken() string {
	return c.token
}

// GetPort implementation for JSONConfiguration
func (c StandardConfiguration) GetPort() string {
	return c.port
}

// GetProjects implementation for JSONProject
func (c StandardConfiguration) GetProjects() []StandardProject {
	return c.projects
}
